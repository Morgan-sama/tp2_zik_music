import { Component } from '@angular/core';
import { register } from 'swiper/element/bundle';
import { changethemeService } from './Services/changetheme.service';

register();

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

  theme : string = 'moon';

  Swtheme(){

    if (this.theme == 'moon'){
      this.theme = 'sunny';
    }else{
      this.theme = 'moon';
    }
  }

  constructor(private thm : changethemeService) {}
  changeThm(){
    this.thm.toggleTheme();
    console.log('je suis là');
    
  }
}
