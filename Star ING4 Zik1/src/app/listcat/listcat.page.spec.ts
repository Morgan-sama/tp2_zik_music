import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ListcatPage } from './listcat.page';

describe('ListcatPage', () => {
  let component: ListcatPage;
  let fixture: ComponentFixture<ListcatPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ListcatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
