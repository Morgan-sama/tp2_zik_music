import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListcatPage } from './listcat.page';

const routes: Routes = [
  {
    path: '',
    component: ListcatPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListcatPageRoutingModule {}
