import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListcatPageRoutingModule } from './listcat-routing.module';

import { ListcatPage } from './listcat.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListcatPageRoutingModule
  ],
  declarations: [ListcatPage]
})
export class ListcatPageModule {}
