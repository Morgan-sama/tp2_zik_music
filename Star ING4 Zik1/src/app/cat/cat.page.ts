import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { LecturePage } from '../lecture/lecture.page';
import { AffichageService } from '../Services/affichage.service';

@Component({
  selector: 'app-cat',
  templateUrl: './cat.page.html',
  styleUrls: ['./cat.page.scss'],
})
export class CatPage implements OnInit {
  Cat: any;
  id: any;
  list: any;

  constructor(public mod : ModalController, public affiche :AffichageService ) { }

  Close(){
    this.mod.dismiss();
    
  }

  chargerObjetCat() {
    const objetString = localStorage.getItem("cat");
    if (objetString) {
      this.Cat = JSON.parse(objetString);
      this.id = this.Cat.idCategorie;
      console.log(this.Cat);   
    }  
  }
  storage5(item :any){
    localStorage.setItem('son',JSON.stringify(item))
  }
  
  
  
  async OpenSong3(){
    const moda = await this.mod.create({
     component : LecturePage
    });
    return await moda.present();
  }

  ngOnInit() {

    this.affiche.getlistOfSongs().subscribe((Response : any )=>{
      console.log(Response);
      this.list = Response;
    });

    this.chargerObjetCat();
  }

}
