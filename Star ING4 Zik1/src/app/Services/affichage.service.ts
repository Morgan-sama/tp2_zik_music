import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AffichageService {

  constructor(private http : HttpClient) { }

  getlistOfSongs(){
    return this.http.get("assets/Songs.json");
  }

  getlistOfCategories(){
    return this.http.get("assets/Categories.json");
  }
}
