import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class changethemeService {
  darkMode: boolean;

  constructor() {
    this.darkMode = localStorage.getItem('darkMode') === 'true';
    this.setTheme();
  }

  setTheme() {
    if (this.darkMode) {
      document.body.classList.add('dark');
    } else {
      document.body.classList.remove('dark');
    }
  }

  toggleTheme() {
    this.darkMode = !this.darkMode;
    localStorage.setItem('darkMode', this.darkMode.toString());
    this.setTheme();
    console.log(localStorage);
    
  }
}

