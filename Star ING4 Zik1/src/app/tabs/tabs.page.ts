import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {

  homeIcon =  'home-outline';
  musicIcon = 'musical-notes-outline' ;
  infoIcon = 'information-circle-outline';

  home(){
    if(this.homeIcon == 'home-outline'){
      this.homeIcon = 'home';

      this.musicIcon = 'musical-notes-outline'
    this.infoIcon = 'information-circle-outline'
    }
    
  }

  music(){
    if(this.musicIcon == 'musical-notes-outline'){
      this.musicIcon = 'musical-notes'

      this.homeIcon = 'home-outline'
    this.musicIcon = 'musical-notes-outline'
    }
    
  }
  
  info(){
    if(this.musicIcon == 'information-circle-outline'){
      this.musicIcon = 'information-circle'

    this.homeIcon = 'home-outline'
    this.musicIcon = 'musical-notes-outline'
    }
   
  }

  constructor() { }

  clear(){

    localStorage.clear();
  }
  ngOnInit() {
  }

}
