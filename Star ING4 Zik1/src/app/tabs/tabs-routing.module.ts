import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsPage } from './tabs.page';
import { HomePage } from '../home/home.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    

    children : [
      {
        path: 'home',
        loadChildren: () => import('../home/home.module').then( m => m.HomePageModule)
      },
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
      },{
        path: 'musiques',
        loadChildren: () => import('../musiques/musiques.module').then( m => m.MusiquesPageModule)
      },{
        path: 'apropos',
        loadChildren: () => import('../apropos/apropos.module').then( m => m.AproposPageModule)
      }
  
    ]
  }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
