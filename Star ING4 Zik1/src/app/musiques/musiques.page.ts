import { Component, OnInit } from '@angular/core';
import { AffichageService } from '../Services/affichage.service';
import { ToastController } from '@ionic/angular';
import { LecturePage } from '../lecture/lecture.page';
import { ModalController } from '@ionic/angular';




@Component({
  selector: 'app-musiques',
  templateUrl: './musiques.page.html',
  styleUrls: ['./musiques.page.scss'],
})
export class MusiquesPage implements OnInit {
selectedTabs = 'Album';
  listAlbums: any;
  listAlbumtri :any ;
  tempon_Albums: any;
  tempon_Albumstri: any;
  is_pending = true ;
  tempon_Albumstri2: any;
  listAlbumtri2: any;

  constructor(private affiche : AffichageService, public mod : ModalController) { }

  list : any [] = [];
  TabAlbum : any [] = [];

  
async OpenSong(){
   const moda = await this.mod.create({
    component : LecturePage
   });
   return await moda.present();
}

  listAlbum(){

    let ids : any []= [];
    
    
    this.affiche.getlistOfSongs().subscribe((Response : any )=>{
        Response.forEach((item: { Album: any[]; }) => {
            item.Album.forEach((album: { idAlbum: any; }) => {
                if(!ids.includes(album.idAlbum)) {
                    ids.push(album.idAlbum);
                    this.TabAlbum.push(item);
                };
            }
            );console.log(this.TabAlbum);
        });
    }
    );
    return this.TabAlbum ;
    
  }

  
  ngOnInit(){
    this.affiche.getlistOfSongs().subscribe( (Response : any )=>{
     console.log(Response);
     this.list = Response;
     this.tempon_Albums = Response;
     this.tempon_Albumstri = this.TabAlbum;
     this.listAlbumtri = this.TabAlbum;
     this.tempon_Albumstri2 = this.TabAlbum;
     this.listAlbumtri2 = this.TabAlbum;
     this.listAlbums = Response;
    //  this.is_pending = false
   });

   
    this.TabAlbum = this.listAlbum();
   
    setTimeout((arg : any) => {this.is_pending = false }, 2600 );
   
    }

    searchItems(ev:any){
      let val = ev.target.value;
       this.listAlbums = this.tempon_Albums.filter((item : any)=>{
        let txtNom =  item.nomArtiste +""+ item.Album;
        return txtNom.toLowerCase().indexOf(val.toLowerCase()) > -1 ;

      })
      console.log(this.listAlbums);
      
    }

    searchItems1(ev:any){
      let val = ev.target.value;
       this.listAlbumtri = this.tempon_Albumstri.filter((item : any)=>{
        let txtNom =  item.nomArtiste +""+ item.Album.nomAlbum;
        return txtNom.toLowerCase().indexOf(val.toLowerCase()) > -1 ;
      })
      console.log(this.listAlbumtri);
      
    }

    searchItems2(ev:any){
      let val = ev.target.value;
       this.listAlbumtri2 = this.tempon_Albumstri2.filter((item : any)=>{
        let txtNom =  item.nomArtiste +""+ item.Album.nomAlbum;
        return txtNom.toLowerCase().indexOf(val.toLowerCase()) > -1 ;
      })
      console.log(this.listAlbumtri2);
      
    }
  
    storage(item : any){
      localStorage.setItem("son",JSON.stringify(item))
      console.log('bien recu')
      console.log(localStorage);
      
    }

    storage2(item : any){
      localStorage.setItem("artiste",JSON.stringify(item))
      console.log('bien recu')
      console.log(localStorage);
      
    }

    storage3(item : any){
      localStorage.setItem("album",JSON.stringify(item))
      console.log('bien recu')
      console.log(localStorage);
      
    }

 


}
