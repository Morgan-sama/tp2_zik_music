import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MusiquesPage } from './musiques.page';

import { SonsParArtistePage } from '../sons-par-artiste/sons-par-artiste.page';
import { SonsParAlbumPage } from '../sons-par-album/sons-par-album.page';

const routes: Routes = [
  {
    path: '',
    component: MusiquesPage
  }
  , {
    path: 'sons-par-artiste',
    component: SonsParArtistePage
  }
  , {
    path: 'sons-par-album',
    component: SonsParAlbumPage
  }
  , {
    path: 'sons-par-album/:idAlbum',
    component: SonsParAlbumPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MusiquesPageRoutingModule {}
