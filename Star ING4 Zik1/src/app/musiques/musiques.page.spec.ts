import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MusiquesPage } from './musiques.page';

describe('MusiquesPage', () => {
  let component: MusiquesPage;
  let fixture: ComponentFixture<MusiquesPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(MusiquesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
