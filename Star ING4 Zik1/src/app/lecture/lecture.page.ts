
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Platform } from '@ionic/angular';
import { NativeAudio, PreloadOptions } from '@capacitor-community/native-audio';
import { AlertController } from '@ionic/angular';





@Component({
  selector: 'app-lecture',
  templateUrl: './lecture.page.html',
  styleUrls: ['./lecture.page.scss'],
})
export class LecturePage implements OnInit {

  son : any ;
  playIcon = 'pause';
  favIcon = 'heart-outline'
  isPlaying = false;
  secs : any;
  
  savedTime: number = 0;
  isLooping = false;
  minutes: any;
  sec: number = 0;
  restant: any;
  currentPosition: number = 0;
  restTime: string = '';
  currentTime:any;
  duration: number = 0;
  chanson : any;
  chemin: any;
  charge:any;
  // audioDuration: Promise<{ duration: number; }> | undefined ;

audioDuration :any
  intervalId: any;
  elapsedTime: any;
  isPause = false;
  res: any;
  path: any;
  volume = 0.5;

  constructor(public mod : ModalController, private alert : AlertController) { }

    Close(){
      this.mod.dismiss();
      clearInterval(this.intervalId);
    }
async presentAlert() {
  const alert = await this.alert.create({
    header: 'Attention',
    subHeader: 'Lecture',
    message: 'Appuyez sur le bouton central pour lancer la lecture, certaines chansons peuvent prendre un certain temps pour se lancer',
    buttons: ['d\'accord']
  });

  await alert.present();
}

    chargerObjet1() {
      const objetString = localStorage.getItem("son");
      if (objetString) {
        this.son = JSON.parse(objetString);
        this.chemin = this.son.lienFichier;
        console.log(this.chemin);
        
      }
     
      console.log(this.son);
      
    }


  ngOnInit() {

    this.presentAlert();
    this.chargerObjet1();
    this.load();
    this.play();

    this.getDuration();
  

    const value : number = this.son.idCategorie;

    switch (value){

      case 1 : 
      this.path = "benskin";
      break;
      case 2 : 
      this.path = "bikutsi";
      break;
      case 3 : 
      this.path = "makossa";
      break;
      case 4 : 
      this.path = "afrourban";
      break;
      default : 
      this.path = "rnb";
      break;
    }

  }


  // async load() {
  //   NativeAudio.preload({
  //     assetId: 'audio',
  //     assetPath: this.chemin 
      
  //   })
  //     .then(() => {
  //       console.log('Fichier audio chargé avec succès');
  //     })
  //     .catch((error) => {
  //       console.error('Erreur lors du chargement du fichier audio :', error);
  //     });
      
  //     this.charge = true
  // }





  





    async load() {
    NativeAudio.preload({
      assetId: 'audio',
      assetPath: "file:///storage/emulated/0/Documents/zik_Ing4(2)/" +this.son.lienFichier,
      audioChannelNum : 1 ,
      isUrl : true  
    })
      .then(() => {
        console.log('Fichier audio chargé avec succès');
      
      })
      .catch((error) => {
        console.error('Erreur lors du chargement du fichier audio :', error);
      });
      
      this.charge = true
  }























  async unload() {
    NativeAudio.unload({
      assetId: 'audio',
    })
      .then(() => {
        console.log('Fichier audio déchargé avec succès');
      })
      .catch((error) => {
        console.error('Erreur lors du déchargement du fichier audio :', error);
      });

      this.charge = false;
  }


  async play(){

    NativeAudio.play({
      assetId: 'audio'
    }).then(() => {
      this.isPlaying = true;
    })
  }

  async playResume() {
   
 if(this.isPlaying == false){
    NativeAudio.resume({
      assetId: 'audio'
    }).then(() => {
      this.isPlaying = true;
    })}

    if (this.isPlaying) {
      NativeAudio.pause({
        assetId: 'audio'
      })
      this.isPlaying = false
      this.isPause = true
    }
  }

  async Loop() {
    NativeAudio.loop({
      assetId: 'audio',
    })
  }
  
  playchange() {
    
    if (this.playIcon =='pause') {
      this.playIcon = 'play'
    } else {
      this.playIcon = 'pause'
    }
  }

  playchange2() {
    
    if (this.favIcon =='heart-outline') {
      this.favIcon = 'heart'
    } else {
      this.favIcon = 'heart-outline'
    }
  }


  getDuration(){


    if(this.charge){
      this.intervalId = setInterval( ()=> {
        NativeAudio.getDuration({
          assetId : 'audio'
        }).then(result => {
          this.duration = Math.floor(result.duration);
          this.restTime = this.convert(this.duration)
        })
        NativeAudio.getCurrentTime({
          assetId : 'audio'
        }).then(result => {
          this.currentPosition = result.currentTime
          this.elapsedTime = this.convert(this.currentPosition)
        })
      })
    }


  }

  convert(ms : number):string{
    const minutes = Math.floor(ms/60);
    const seconds = ((ms % 600)/ 10).toFixed(0);
     this.res = minutes + ":" + (Number(seconds) < 10 ? '0' : '') +seconds ;
    return this.res;
  }

  changePosition(){
    if(this.charge){
      NativeAudio.play({
        assetId : 'audio',
        time : this.currentPosition
      });
      this.isPlaying = false;
      this.isPause = true ;
    }
  }

  UpVolume(){

    if(this.volume < 0.9){
      this.volume += 0.1
      NativeAudio.setVolume({
        assetId : 'audio',
        volume : this.volume,
      });
    }

  }

  DownVolume(){

    if(this.volume > 0.1){
      this.volume =  this.volume - 0.1
      NativeAudio.setVolume({
        assetId : 'audio',
        volume : this.volume,
      });
    }

  }

  

 
  

}
