import { Component, OnInit } from '@angular/core';
import { ListcatPage } from '../listcat/listcat.page';
import { ModalController } from '@ionic/angular';
import { AffichageService } from '../Services/affichage.service';
import { Device } from '@capacitor/device';
import { TranslateService } from '@ngx-translate/core';
import { LecturePage } from '../lecture/lecture.page';
import { SonsParArtistePage } from '../sons-par-artiste/sons-par-artiste.page';
import { SonParArtisteFavoriPage } from '../son-par-artiste-favori/son-par-artiste-favori.page';
import { CatPage } from '../cat/cat.page';



@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  is_pending = true ;

  constructor(public mod : ModalController,private affiche : AffichageService,private translate: TranslateService) {
    
  }
  list : any [] = [];
  listCat : any [] = [];
  


  TabAlbum : any [] = [];
  TabFav : any [] = [];
  listArtFav : any [] = [];



  async OpenCat(){
    const moda = await this.mod.create({
     component : CatPage
    });
    return await moda.present();
 }

  async OpenSong(){
    const moda = await this.mod.create({
     component : LecturePage
    });
    return await moda.present();
 }

 async OpenSong2(){
  const moda = await this.mod.create({
   component : SonParArtisteFavoriPage
  });
  return await moda.present();
}

storage3(tof : any){
  localStorage.setItem("artfav",JSON.stringify(tof))
  console.log('bien recu')
  console.log(localStorage);
  
}

 storage(fav : any){
  localStorage.setItem("son",JSON.stringify(fav))
  console.log('bien recu')
  console.log(localStorage);
  
}

storage5(item :any){
  localStorage.setItem('son',JSON.stringify(item))
}



async OpenSong3(){
  const moda = await this.mod.create({
   component : LecturePage
  });
  return await moda.present();
}

storageCat(cat :any){
  localStorage.setItem('cat',JSON.stringify(cat))
}

  listFav(){

    
    this.affiche.getlistOfSongs().subscribe((Response : any )=>{
      this.TabFav = Response.filter((song: { est_favori: any; }) => song.est_favori);
  });
    
    return this.TabFav ;

  }

  


  listAlbum(){

    let ids : any []= [];
    
    
    this.affiche.getlistOfSongs().subscribe((Response : any )=>{
        Response.forEach((item: { Album: any[]; }) => {
            item.Album.forEach((album: { idAlbum: any; }) => {
                if(!ids.includes(album.idAlbum)) {
                    ids.push(album.idAlbum);
                    this.TabAlbum.push(item);
                };
            }
            );console.log(this.TabAlbum);
        });
    }
    );
    return this.TabAlbum ;
  }


  listArtistesFav(){

    
      this.affiche.getlistOfSongs().subscribe((response: any) => {
         this.listArtFav = response.filter((song: any) => song.score > 8);
      });
      

      console.log(this.listArtFav);
      
    
  }

  changeLanguage(lang: string) {
    this.translate.use(lang);
  }

  async handleDevice(){
      
    const code = await Device. getLanguageCode(); 
    console.log('Device code:', code);
    
    const deviceInfo = await Device. getInfo(); 
    console.log('Device info:', deviceInfo);
    
    const deviceId = await Device. getId(); 
    console.log(' Device id:', deviceId);
    }

  

  ngOnInit(): void {

    this.handleDevice();
    
    this.affiche.getlistOfSongs().subscribe((Response : any )=>{
     console.log(Response);
     this.list = Response;
   });

   this.affiche.getlistOfCategories().subscribe((Response : any )=>{
    console.log(Response);
    this.listCat = Response;
  });

  this.TabAlbum = this.listAlbum();
  this.TabFav = this.listFav();

  this. listArtistesFav();
  
  setTimeout((arg : any) => {this.is_pending = false }, 1600 );
    }
    
}
