import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SonsParAlbumPage } from './sons-par-album.page';

describe('SonsParAlbumPage', () => {
  let component: SonsParAlbumPage;
  let fixture: ComponentFixture<SonsParAlbumPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(SonsParAlbumPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
