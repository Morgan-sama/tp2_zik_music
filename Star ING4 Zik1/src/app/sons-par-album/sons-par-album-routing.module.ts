import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SonsParAlbumPage } from './sons-par-album.page';

const routes: Routes = [
  {
    path: '',
    component: SonsParAlbumPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SonsParAlbumPageRoutingModule {}
