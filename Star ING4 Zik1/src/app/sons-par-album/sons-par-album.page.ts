import { Component, OnInit } from '@angular/core';
import { AffichageService } from '../Services/affichage.service';

@Component({
  selector: 'app-sons-par-album',
  templateUrl: './sons-par-album.page.html',
  styleUrls: ['./sons-par-album.page.scss'],
})
export class SonsParAlbumPage implements OnInit {
  albums: any;
  nom: any;
  list: any;
  selectedAlbum: any;
  

  constructor(private service : AffichageService) { }

  ngOnInit() {
    this.service.getlistOfSongs().subscribe( (Response : any )=>{
      console.log(Response);
      this.list = Response.Album;
      
      
    });
    this.chargerObjet();
  }

  chargerObjet() {
    const objetString = localStorage.getItem("album");
    if (objetString) {
      this.selectedAlbum = JSON.parse(objetString);
    }
    console.log(this.selectedAlbum.Album);
    this.nom = this.selectedAlbum.Album;
    console.log(this.nom);
    
  }

}
