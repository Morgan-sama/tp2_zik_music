import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SonsParAlbumPageRoutingModule } from './sons-par-album-routing.module';

import { SonsParAlbumPage } from './sons-par-album.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SonsParAlbumPageRoutingModule
  ],
  declarations: [SonsParAlbumPage]
})
export class SonsParAlbumPageModule {}
