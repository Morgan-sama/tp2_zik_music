import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SonParArtisteFavoriPageRoutingModule } from './son-par-artiste-favori-routing.module';

import { SonParArtisteFavoriPage } from './son-par-artiste-favori.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SonParArtisteFavoriPageRoutingModule
  ],
  declarations: [SonParArtisteFavoriPage]
})
export class SonParArtisteFavoriPageModule {}
