import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SonParArtisteFavoriPage } from './son-par-artiste-favori.page';

const routes: Routes = [
  {
    path: '',
    component: SonParArtisteFavoriPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SonParArtisteFavoriPageRoutingModule {}
