import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SonParArtisteFavoriPage } from './son-par-artiste-favori.page';

describe('SonParArtisteFavoriPage', () => {
  let component: SonParArtisteFavoriPage;
  let fixture: ComponentFixture<SonParArtisteFavoriPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(SonParArtisteFavoriPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
