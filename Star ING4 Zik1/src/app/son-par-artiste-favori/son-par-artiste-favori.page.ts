import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AffichageService } from '../Services/affichage.service';
import { LecturePage } from '../lecture/lecture.page';
@Component({
  selector: 'app-son-par-artiste-favori',
  templateUrl: './son-par-artiste-favori.page.html',
  styleUrls: ['./son-par-artiste-favori.page.scss'],
})
export class SonParArtisteFavoriPage implements OnInit {
  artistefav: any;
  nomfav: any;
  list: any;
  photo: any;

  constructor(public mod : ModalController, private service : AffichageService) { }
  
  chargerObjet2() {
    const objetString = localStorage.getItem("artfav");
    if (objetString) {
      this.artistefav = JSON.parse(objetString);
    }
    console.log(this.artistefav);
    this.nomfav = this.artistefav.nomArtiste ;
    this.photo = this.artistefav.photoArtiste
    
  }
  Close(){
    this.mod.dismiss();
  }

  ngOnInit() {
    this.service.getlistOfSongs().subscribe( (Response : any )=>{
      console.log(Response);
      this.list = Response;
      
    });

    this.chargerObjet2();
  }

  storage(item :any){
    localStorage.setItem('son',JSON.stringify(item))
  }

  async OpenSong(){
    const moda = await this.mod.create({
     component : LecturePage
    });
    return await moda.present();
 }
}
