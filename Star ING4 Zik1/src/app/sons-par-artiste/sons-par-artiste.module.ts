import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SonsParArtistePageRoutingModule } from './sons-par-artiste-routing.module';

import { SonsParArtistePage } from './sons-par-artiste.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SonsParArtistePageRoutingModule
  ],
  declarations: [SonsParArtistePage]
})
export class SonsParArtistePageModule {}
