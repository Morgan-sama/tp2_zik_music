import { Component, OnInit } from '@angular/core';
import { AffichageService } from '../Services/affichage.service';
import { ModalController } from '@ionic/angular';
import { LecturePage } from '../lecture/lecture.page';

@Component({
  selector: 'app-sons-par-artiste',
  templateUrl: './sons-par-artiste.page.html',
  styleUrls: ['./sons-par-artiste.page.scss'],
})
export class SonsParArtistePage implements OnInit {
  artiste: any;
  list: any;
  nom : any;
  artistefav: any;
  nomfav: any;

  
  constructor(private service : AffichageService, public mod : ModalController) { }

  async OpenSong(){
    const moda = await this.mod.create({
     component : LecturePage
    });
    return await moda.present();
  }

  ngOnInit() {
    this.service.getlistOfSongs().subscribe( (Response : any )=>{
      console.log(Response);
      this.list = Response;
      console.log(this.nom);
      
      
      
    });

    this.chargerObjet();
   

  }

  chargerObjet() {
    const objetString = localStorage.getItem("artiste");
    if (objetString) {
      this.artiste = JSON.parse(objetString);
    }
    console.log(this.artiste);
    this.nom = this.artiste.nomArtiste ;
    
  }

  storage6(item : any){
    localStorage.setItem("son",JSON.stringify(item))
    console.log('bien recu')
    console.log(localStorage);
    
  }

}

