import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SonsParArtistePage } from './sons-par-artiste.page';

describe('SonsParArtistePage', () => {
  let component: SonsParArtistePage;
  let fixture: ComponentFixture<SonsParArtistePage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(SonsParArtistePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
