import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SonsParArtistePage } from './sons-par-artiste.page';

const routes: Routes = [
  {
    path: '',
    component: SonsParArtistePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SonsParArtistePageRoutingModule {}
