import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'tabs',
    pathMatch: 'full'
  },
  {
    path: 'tabs',
    loadChildren: () => import('./tabs/tabs.module').then( m => m.TabsPageModule)
  }, 
  {
    path: 'musiques',
    loadChildren: () => import('./musiques/musiques.module').then( m => m.MusiquesPageModule)
  },
  {
    path: 'apropos',
    loadChildren: () => import('./apropos/apropos.module').then( m => m.AproposPageModule)
  },
  {
    path: 'listcat',
    loadChildren: () => import('./listcat/listcat.module').then( m => m.ListcatPageModule)
  },
  
  {
    path: 'languages',
    loadChildren: () => import('./languages/languages.module').then( m => m.LanguagesPageModule)
  },
  {
    path: 'sons-par-artiste',
    loadChildren: () => import('./sons-par-artiste/sons-par-artiste.module').then( m => m.SonsParArtistePageModule)
  },
  {
    path: 'sons-par-album',
    loadChildren: () => import('./sons-par-album/sons-par-album.module').then( m => m.SonsParAlbumPageModule)
  },
  {
    path: 'lecture',
    loadChildren: () => import('./lecture/lecture.module').then( m => m.LecturePageModule)
  },
  {
    path: 'son-par-artiste-favori',
    loadChildren: () => import('./son-par-artiste-favori/son-par-artiste-favori.module').then( m => m.SonParArtisteFavoriPageModule)
  },
  {
    path: 'cat',
    loadChildren: () => import('./cat/cat.module').then( m => m.CatPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
