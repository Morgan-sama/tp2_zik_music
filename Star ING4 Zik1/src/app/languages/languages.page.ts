import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import * as en from '../../assets/i18n/en.json';
import * as es from '../../assets/i18n/es.json';

@Component({
  selector: 'app-languages',
  templateUrl: './languages.page.html',
  styleUrls: ['./languages.page.scss'],
})
export class LanguagesPage implements OnInit {

  

  clicked1 = false ;
  clicked2 = false ;
  toggle1Value = false;
  toggle2Value = false;

  onToggle1Change() {
    if (this.toggle1Value) {
      this.toggle2Value = false;
    }
  }

  onToggle2Change() {
    if (this.toggle2Value) {
      this.toggle1Value = false;
    }
  }

  l1 = 'en';
  l2 = 'es'

  constructor( private translate : TranslateService) { 
    this.translate.setTranslation('en', en);
    this.translate.setTranslation('es', es);
  }

  

  Switch(){
    
      this.changeLanguage(this.l1)

  }
  Switch2(){

    this.changeLanguage(this.l2)
    
}

changeLanguage(language: string) {
  this.translate.use(language);
}

  ngOnInit() {
  }

}
