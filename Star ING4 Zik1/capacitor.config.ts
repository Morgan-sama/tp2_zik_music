import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'Zik Musik Morgan_Claudel_ANGO',
  webDir: 'www',
  server: {
    androidScheme: 'https'
  }
};

export default config;
